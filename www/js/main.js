    var pictureSource;   // picture source
    var destinationType; // sets the format of returned value

    // Wait for device API libraries to load
    //
    document.addEventListener("deviceready",onDeviceReady,false);

    // device APIs are available
    //
    function onDeviceReady() {
        pictureSource = navigator.camera.PictureSourceType;
        destinationType = navigator.camera.DestinationType;

        $('#connection-type').text('Tipo de conexão de rede: ' + getConnectionType());
    }

    function enviarImagem(imageData)
    {
      $.ajax({
        url: "http://192.168.200.109/guarda-fotos/guarda-foto.php",  // URL para acessar via AJAX
        type: "post", // método de envio dos dados (POST, GET)
        data: {foto: imageData}, // dados a enviar na requisição AJAX
        dataType: "json", // tipo de retorno esperado
        success: function(json) {  // função a ser executada em caso de sucesso da requisição
          alert(json.msg);
        },
        error: function(xhr, errorMessage) { // função a ser executada em caso de erro na requisição
          alert('Erro: ' + errorMessage);
        }
      });
    }

    // Called when a photo is successfully retrieved
    //
    function onPhotoDataSuccess(imageData) {
      // cria um novo elemento img (html)
      var image = newImage();
      image.attr('src', "data:image/jpeg;base64," + imageData);

      enviarImagem(imageData);
    }

    encodeImageUri = function(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function() {
            c.width = this.width;
            c.height = this.height;
            ctx.drawImage(img, 0, 0);

            if(typeof callback === 'function'){
                var dataURL = c.toDataURL("image/jpeg");
                callback(dataURL.slice(22, dataURL.length));
            }
        };
        img.src = imageUri;
    }

    // Called when a photo is successfully retrieved
    //
    function onPhotoURISuccess(imageURI) {
      var image = newImage();
      image.attr('src', imageURI);

      imageData = encodeImageUri(imageURI, enviarImagem)
    }

    // Create a new <img> element and appends it on container
    //
    function newImage() {
      var image = $('<img>');
      image.addClass('img-thumbnail');
      image.attr('width', '300px;');
      image.attr('height', '300px;');
      image.appendTo($('#images'));

      return image;
    }

    // A button will call this function
    //
    function capturePhoto() {
      // Take picture using device camera and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess,
        onFail, { quality: 100, saveToPhotoAlbum: true,
        destinationType: destinationType.DATA_URL
         });
    }

    // A button will call this function
    //
    function capturePhotoEdit() {
      // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoURISuccess,
        onFail, { quality: 50, allowEdit: true,
        destinationType: destinationType.FILE_URI });
    }

    // A button will call this function
    //
    function getPhoto(source) {
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 100,
        destinationType: destinationType.FILE_URI,
        sourceType: source });
    }

    // Called if something bad happens.
    //
    function onFail(message) {
      alert('Falha: ' + message);
    }

    // Identify network connection type
    function getConnectionType()
    {
      var states = {};
      states[Connection.UNKNOWN]  = 'Conexão desconhecida';
      states[Connection.ETHERNET] = 'Conexão Ethernet';
      states[Connection.WIFI]     = 'Conexão WiFi';
      states[Connection.CELL_2G]  = 'Conexão 2G';
      states[Connection.CELL_3G]  = 'Conexão 3G';
      states[Connection.CELL_4G]  = 'Conexão 4G';
      states[Connection.CELL]     = 'Conexão celular genérica';
      states[Connection.NONE]     = 'Sem conexão de rede';

      return states[navigator.connection.type];
    }

    function sendFile(file)
    {
      $.ajax({
        type: 'post',
        url: 'http://192.168.200.109/guarda-fotos/guarda-foto.php',
        data: file,
        dataType: json,
        success: function (json) {
          alert(json.msg);
        },
        xhrFields: {
          // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
          onprogress: function (progress) {
            // calculate upload progress
            var percentage = Math.floor((progress.total / progress.totalSize) * 100);
            // log upload progress to console
            console.log('progress', percentage);
            if (percentage === 100) {
              console.log('DONE!');
            }
          }
        },
        processData: false,
        contentType: file.type
      });
    }

    function teste()
    {
      $.ajax({
        url: "http://192.168.200.109/guarda-fotos/teste.php",  // URL para acessar via AJAX
        type: "post", // método de envio dos dados (POST, GET)
        data: {msg: "Teste de comunicação app/webservice"}, // dados a enviar na requisição AJAX
        dataType: "json", // tipo de retorno esperado
        success: function(json) {  // função a ser executada em caso de sucesso da requisição
          alert(json.msg);
        },
        error: function(xhr, errorMessage) { // função a ser executada em caso de erro na requisição
          alert('Erro: ' + errorMessage);
        }
      });
    }
	
	
	 function photoGo(source) {
            // Retrieve image file location from specified source
            navigator.camera.getPicture(
                uploadPhoto,
                function(message) { alert('get picture failed'); },
                {
                    quality         : 50,
                    destinationType : navigator.camera.DestinationType.FILE_URI,
                    sourceType      : source
                }
            );
        }

        function uploadPhoto(imageURI) {
            var options = new FileUploadOptions();
            options.fileKey="file";
            options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
            options.mimeType="image/jpeg";

            var params = {};
            params.value1 = "test";
            params.value2 = "param";

            options.params = params;

            var ft = new FileTransfer();
            ft.upload(imageURI, encodeURI("http://192.168.13.12/app/upload.php"), win, fail, options);
			alert("ok");
        }

        function win(r) {
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
			alert ("win");
        }

        function fail(error) {
            alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
			alert ("fail");
        }